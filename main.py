from helper import check_diag, check_diag_2, check_diag_3, check_diag_4
help(check_diag)
help(check_diag_2)
help(check_diag_3)
help(check_diag_4)
help(print)


print("Добро пожаловать в игру крестики-нолики!")
letter1 = ''
letter2 = ''
letter1 = input("Игрок 1, каким символом будете играть: X или 0? Выберите одну из двух переменных: ")
while letter1 != "X" and letter1 != "0":
    letter1 = input("Введена неверная переменная! Введите X или 0: ")
if letter1 == 'X':
    letter2  ='0'
else:
    letter2 = 'X'
print(f'Игрок 2, вы будете играть символом {letter2}')
while True:
    try:
        n = int(input("Введите натуральное число n, где n - количество строчек и количество столбцов поля: "))
    except ValueError:
        print("Вы что-то напутали!Нужно ввести натуральное число!")
    except TypeError:
        print("Что-то пошло не так")
    else:
        if n > 0:
            break
        else:
            print("Вы ввели не натуральное число!")
krest_nol = [['.'] * n for i in range(n)]
flag = False
y = n * n
while y >= 0:
    print("Ход игрока X")
    with open('Failed', 'a') as file:
        file.write('Ход игрока X\n')
    a = "X"
    while True:
        try:
            n1 = int(input("Введите номер строки в виде натурального числа: "))
        except ValueError:
            print("Вы что-то напутали!Нужно ввести натуральное число!")
        except TypeError:
            print("Что-то пошло не так")
        else:
            if n1 > 0:
                break
            else:
                print("Вы ввели не натуральное число!")
    while n1 > n or n1 < 1:
        print("Вы вышли из диапазона поля!Введите заново номер строки в виде числа")
        n1 = int(input("Введите номер строки в виде числа: "))
    while True:
        try:
            n2 = int(input("Введите номер столбца в виде натурального числа: "))
        except ValueError:
            print("Вы что-то напутали!Нужно ввести натуральное число!")
        except TypeError:
            print("Что-то пошло не так")
        else:
            if n2 > 0:
                break
            else:
                print("Вы ввели не натуральное число!")
    while n2 > n or n2 < 1:
        print("Вы вышли из диапазона поля!Введите заново номер столбца в виде числа")
        n2 = int(input("Введите номер столбца в виде числа: "))
    if krest_nol[n1 - 1][n2 - 1] == '.':
        krest_nol[n1 - 1][n2 - 1] = "X"
    else:
        while krest_nol[n1 - 1][n2 - 1] != '.':
            print("Выбранная Вами ячейка уже занята")
            while True:
                try:
                    n1 = int(input("Введите номер строки в виде натурального числа: "))
                    n2 = int(input("Введите номер столбца в виде натурального числа: "))
                except ValueError:
                    print("Вы что-то напутали!Нужно ввести натуральные числа!")
                except TypeError:
                    print("Что-то пошло не так")
                else:
                    if n2 > 0 and n1 > 0:
                        break
                    else:
                        print("Вы ввели не натуральные числа!")
        krest_nol[n1 - 1][n2 - 1] = "X"
    for i in krest_nol:
        print(*i)
    with open('Failed', 'a') as file:
        for i in range(n):
            for j in range(n):
                file.write(krest_nol[i][j])
            file.write('\n')
    y -= 1
    if check_diag(krest_nol, n, "0"):
        print("Выиграл игрок: 0")
        break
    elif check_diag(krest_nol, n, "X"):
        print("Выиграл игрок: X")
        break
    if check_diag_4(krest_nol, n, "0"):
        print("Выиграл игрок: 0")
        break
    elif check_diag_4(krest_nol, n, "X"):
        print("Выиграл игрок: X")
        break
    if check_diag_2(krest_nol, n, "X"):
        print("Выиграл игрок: X")
        break
    elif check_diag_2(krest_nol, n, "0"):
        print("Выиграл игрок: 0")
        break
    if check_diag_3(krest_nol, n, "X"):
        print("Выиграл игрок: X")
        break
    elif check_diag_3(krest_nol, n, "0"):
        print("Выиграл игрок: 0")
        break
    if y == 0:
        print("Ничья")
        break
    print("Ход игрока 0")
    with open('Failed', 'a') as file:
        file.write('Ход игрока 0\n')
    a = "0"
    while True:
        try:
            n1 = int(input("Введите номер строки в виде натурального числа: "))
        except ValueError:
            print("Вы что-то напутали!Нужно ввести натуральное число!")
        except TypeError:
            print("Что-то пошло не так")
        else:
            if n1 > 0:
                break
            else:
                print("Вы ввели не натуральное число!")
    while n1 > n or n1 < 1:
        print("Вы вышли из диапазона поля!Введите заново номер строки в виде числа")
        n1 = int(input("Введите номер строки в виде числа:"))
    while True:
        try:
            n2 = int(input("Введите номер столбца в виде натурального числа: "))
        except ValueError:
            print("Вы что-то напутали!Нужно ввести натуральное число!")
        except TypeError:
            print("Что-то пошло не так")
        else:
            if n2 > 0:
                break
            else:
                print("Вы ввели не натуральное число!")
    while n2 > n or n2 < 1:
        print("Вы вышли из диапазона поля!Введите заново номер столбца в виде числа")
        n2 = int(input("Введите номер столбца в виде числа: "))
    if krest_nol[n1 - 1][n2 - 1] == '.':
        krest_nol[n1 - 1][n2 - 1] = "0"
    else:
        while krest_nol[n1 - 1][n2 - 1] != '.':
            print("Выбранная Вами ячейка уже занята")
            while True:
                try:
                    n1 = int(input("Введите номер строки в виде натурального числа: "))
                    n2 = int(input("Введите номер столбца в виде натурального числа: "))
                except ValueError:
                    print("Вы что-то напутали!Нужно ввести натуральные числа!")
                except TypeError:
                    print("Что-то пошло не так")
                else:
                    if n2 > 0 and n1 > 0:
                        break
                    else:
                        print("Вы ввели не натуральные числа!")
        krest_nol[n1 - 1][n2 - 1] = "0"
    for i in krest_nol:
        print(*i)
    with open('Failed', 'a') as file:
        for i in range(n):
            for j in range(n):
                file.write(krest_nol[i][j])
            file.write('\n')
    y -= 1
    if check_diag(krest_nol, n, "X"):
        print("Выиграл игрок: X")
        break
    elif check_diag(krest_nol, n, "0"):
        print("Выиграл игрок: 0")
        break
    if check_diag_4(krest_nol, n, "X"):
        print("Выиграл игрок: X")
        break
    elif check_diag_4(krest_nol, n, "0"):
        print("Выиграл игрок: 0")
        break
    if check_diag_2(krest_nol, n, "0"):
        print("Выиграл игрок: 0")
        break
    elif check_diag_2(krest_nol, n, "X"):
        print("Выиграл игрок: X")
        break
    if check_diag_3(krest_nol, n, "0"):
        print("Выиграл игрок: 0")
        break
    elif check_diag_3(krest_nol, n, "X"):
        print("Выиграл игрок: X")
        break
    if y == 0:
        print("Ничья")
        break
