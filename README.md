# Personal repository with the "Tic-Tac-Toe" project
This repository includes the "Tic-Tac-Toe" project using functions, exceptions, saving intermediate game results to a file, and dividing the program itself into files.  
This project allows two people to play the famous Tic-Tac-Toe game from one computer.  
**The game includes:**
* checking the winnings of the players along the main diagonal
* checking the winnings of the players along the side diagonal
* checking the winnings of the players horizontally
* checking the winnings of the players vertically
* the ability for players to choose variables 0 or X  
_**Intermediate game results are saved in a file:**_  
You can see how to save program results to files using this link:  
[instructions for saving to file](https://stackoverflow.com/questions/9536714/python-save-to-file/9536741)  
# Rules of the game:
The players take turns putting signs on the empty squares of the field (one is always crosses, the other is always zeroes). The first one to line up "n" of his pieces vertically, horizontally or diagonally wins. The first move is made by the player placing crosses.
# Instructions: "How to deploy a project locally"  
**Steps:**
1. Install git
2. Find and copy the "clone" button on the top right corner of the repository page
3. Type in the command line _git clone "link to your repository"_, being in the directory where this project will be located
4. Go to the created directory with the cloned repository _cd "name of the created directory"_
# Additional sources that helped in creating the game:
* [python exception handling](https://pythonworld.ru/tipy-dannyx-v-python/isklyucheniya-v-python-konstrukciya-try-except-dlya-obrabotki-isklyuchenij.html)
* ![image with python exception handing](https://pythonru.com/wp-content/uploads/2020/02/sintaksis-obrabotki-isklyuchenij.png)