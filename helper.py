def check_diag(krest_nol: str, n: int, a: str) -> bool:
    """
    Return the winnings of one of the players, if it happened
    This function runs along the main diagonal of the matrix and checks if there are identical symbols in each cell


    :param n: number of rows/ number of columns
    :param a: symbol X or 0
    :param krest_nol: matrix
    :return: False or True
    """
    for i in range(n):
        if krest_nol[i][i] != a:
            return False
    return True


def check_diag_2(krest_nol: str, n: int, a: str) -> bool:
    """
    Return the winnings of one of the players, if it happened
    This function runs along the line by line of the matrix and checks if there are identical sembols in each cell


    :param n: number of rows/ number of columns
    :param a: symbol X or 0
    :param krest_nol: matrix
    :return: False or True
    """
    for i in range(n):
        flag = True
        for j in range(n):
            if krest_nol[i][j] != a:
                flag = False
                break
        if flag:
            return True
    return False


def check_diag_3(krest_nol: str, n: int, a: str) -> bool:
    """
    Return the winnings of one of the players, if it happened
    This function runs along the by columns of the matrix and checks if there are identical sembols in each cell


    :param n: number of rows/ number of columns
    :param a: symbol X or 0
    :param krest_nol: matrix
    :return: False or True
    """
    for i in range(n):
        flag = True
        for j in range(n):
            if krest_nol[j][i] != a:
                flag = False
                break
        if flag:
            return True
    return False


def check_diag_4(krest_nol: str, n: int, a: str) -> bool:
    """
    Return the winnings of one of the players, if it happened
    This function runs along the collateral diagonal of the matrix and checks if there are identical sembols in each cell


    :param n: number of rows/ number of columns
    :param a: symbol X or 0
    :param krest_nol: matrix
    :return: False or True
    """
    for i in range(n):
        if krest_nol[i][n - i - 1] != a:
            return False
    return True


def print(self, *args, sep=' ', end='\n', file=None):
    pass